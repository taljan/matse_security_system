
## Abgabe MatseProjekt 21.01.2019 - Tolga Ayvazoglu

## Starten des Projekts.

Es gibt zwei Möglichkeiten dieses Projekt einzurichten.

1. GitBash öffnen.
2. In den gewünschten Pfad navigieren und folgendes asuführen:
git clone git@bitbucket.org:taljan/matse_security_system.git

3. IDE Starten, völlig egal welches aber bevorzugt ist Intellij
4. Projekt Öffnen
5. Fertig.

ODER das Projekt befindet sich bereits in einem USB Stick. :)

---

## Programm ausführen

Alle Testdateien sind im Projekt hinzugefügt, mit dem Namen "Room_x.txt". Um die gewünschten Daten zu laden
sollte in der Main Methode den Methodenparameter         
````readDataFromFile("Room_1.txt");````
ändern um alle Daten einzeln auszulesen.

## Bekannte Bugs

1. Es ist leider ein bekannter Bug vorhanden. Das Programm Endet nicht, wann es sollte.
Es wurde ein Workaround implementiert allerdings ist das nicht sehr zufriedenstellend.
Durch diesen Bug fällt es leider dazu, dass Sonderfälle nicht abgedeckt werden können.
Lösungen die nicht gefunden werden können, welche durchaus möglich sind, können durch diesen 
Bug leider nicht abgefangen werden.

2. Manchmal passiert es das eine Vitrine übersehen wird - ich hatte leider keine Zeit
um diesen Bug zu beheben. Die vermutung liegt darin, dass die Vitrinen Punkte
nicht korrekt gesetzt werden, da die Diagonalen der Felder bei der Initialisierung gesetzt werden.
Es kann sein, dass bei der Berechnung ein kleiner Fehler auftritt.

## Beheben der Bugs
1. Einfach nicht beachten :-P
