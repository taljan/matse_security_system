package de.hhs.matse.tolga.Data;

public class Node {

    private int rowIndex;
    private int colIndex;
    private boolean isBlocked;

    public Node(int rowIndex, int colIndex) {
        this.rowIndex = rowIndex;
        this.colIndex = colIndex;
    }

    public boolean isBlocked() {
        return isBlocked;
    }
    
    public void setBlocked(boolean isBlock) {
        isBlocked = isBlock;
    }

    public void setRowIndex(int rowIndex) {
        this.rowIndex = rowIndex;
    }

    public void setColIndex(int colIndex) {
        this.colIndex = colIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    public int getColIndex() {
        return colIndex;
    }

    /**
     * Berechnet ob der aktuelle Knoten tatsächlich an einer Wand is.
     *
     * @param searchSpace Der Raum, welcher untersucht werden soll.
     * @return <code>true</code> wenn eine Wand gefunden wurde.
     */
    public boolean isAtWall(Node[][] searchSpace) {
        if (getRowIndex() == 0) {
            return true;
        } else if (getColIndex() == 0) {
            return true;
        }
        else if (getColIndex() == searchSpace[searchSpace.length - 1].length - 1) {
            return true;
        }
        else return getRowIndex() == (searchSpace.length - 1);

    }

    /**
     * Gibt <code>true</code> oder <code>false</code> zurück,
     * wenn der aktuelle Knoten parallel zum vorherigen Knoten ist.
     *
     * @param startNode der Knoten indem sich der Algorithmus gerade befindet.
     * @param searchSpace der Raum der untersucht werden soll.
     * @return <code>true</code> wenn beide an der selben Wand sind.
     */
    public boolean isAtSameWallWith(Node startNode, Node[][] searchSpace) {
        if ((getColIndex() >= 0 && getRowIndex() == 0) && (startNode.getColIndex() >= 0 && startNode.getRowIndex() == 0)) {
            return true;
        }
        else if ((getColIndex() == 0 && getRowIndex() >= 0) && (startNode.getColIndex() == 0 && startNode.getRowIndex() >= 0)) {
            return true;
        }
        else if ((getColIndex() == searchSpace[searchSpace.length - 1].length - 1 && getRowIndex() >= 0) &&
                startNode.getColIndex() == searchSpace[searchSpace.length - 1].length - 1 && startNode.getRowIndex() >= 0) {
            return true;
        }
        else return (getRowIndex() == searchSpace.length - 1 && getColIndex() >= 0) &&
                    startNode.getRowIndex() == searchSpace.length - 1 && startNode.getColIndex() >= 0;
    }

    @Override
    public String toString() {
        return "[x: " + colIndex +
                "; y:" + rowIndex +
                "]";
    }

}
