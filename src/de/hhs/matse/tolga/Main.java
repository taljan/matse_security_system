package de.hhs.matse.tolga;

import de.hhs.matse.tolga.Data.Node;
import de.hhs.matse.tolga.algorithm.MasterAlgorithmOfTheUniverse;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    private static List<Node> roomCoordinates = new ArrayList<>();
    
    public static void main(String[] args) {
        readDataFromFile("Room_1.txt");
    
        showRoomCoordinates(roomCoordinates);
    
        System.out.println("Der Laser startet bei: x:0, y:0");
        MasterAlgorithmOfTheUniverse algo = new MasterAlgorithmOfTheUniverse(roomCoordinates);
        algo.searchForPathInSearchSpace();
    }
    
    private static void showRoomCoordinates(List<Node> roomCoordinates) {
        List<Node> room = new ArrayList<>();
        room.addAll(roomCoordinates);
        
        System.out.println("Wand: " + "[0, 0]");
        System.out.println("Wand: " + "[0, " + (room.get(0).getColIndex()) + "]");
        System.out.println("Wand: " + "[0, " + (room.get(0).getRowIndex()) + "]");
        System.out.println("Wand: " + "["+ room.get(0).getRowIndex() + ", " + (room.get(0).getColIndex()) + "]");
    
        room.remove(0);
        room.forEach(showCase -> System.out.println("Vitrine: [" + showCase.getRowIndex() + "," + showCase.getColIndex() + "]"));
    }
    
    private static void readDataFromFile(String fileName) {
        try {
            File file = new File(fileName);
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                String[] result = string.split("[^0-9]+");
                
                List<String> vectorStrings = Arrays.asList(result);
                
                if (result.length > 0) {
                    roomCoordinates.add(computeVectorsOfVectorStrings(vectorStrings));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    private static Node computeVectorsOfVectorStrings(List<String> vectorStrings) {
        double x = Double.parseDouble(vectorStrings.get(1));
        double y = Double.parseDouble(vectorStrings.get(2));
        
        return new Node((int) x, (int) y);
    }
    
}
