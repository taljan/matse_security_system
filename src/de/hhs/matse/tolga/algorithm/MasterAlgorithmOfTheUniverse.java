package de.hhs.matse.tolga.algorithm;

import de.hhs.matse.tolga.Data.Node;

import java.awt.geom.Line2D;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

public class MasterAlgorithmOfTheUniverse {

    private int yMultiplier = 1;
    private int xMultiplier = 1;
    private boolean countingY = true;

    private Node previousNode;
    private Node targetNode;

    private Node[][] searchSpace;
    private Set<Node> visitedNodes;
    private Queue<Node> notVisitedNodes;

    /**
     * Der Konstruktor des Algorithmus. Nimmt jeglichen Daten entgegen und erstellt
     * die nötigen Datenstrukturen, um den Suchprozess durchführen zu können.
     *
     * Der Raum wir anhand der mitgegeben Daten erstellt.
     * Eine List von 'notVisitedNodes' wird erstellt, damit wir später ein Abbruchbedingung haben.
     * Eine List von 'visitedNodes' wird erstellt, damit wir den letzten Knoten verfolgen können und nicht wiederholt
     * and die gleiche Stelle schießen.
     *
     * @param roomCoordinates ArrayList<Node> Beinhaltet die Daten für die Breit und Höhe des 2DRaumes und VitrinenPositionen.
     */
    public MasterAlgorithmOfTheUniverse(List<Node> roomCoordinates) {
        int roomHeight = roomCoordinates.get(0).getRowIndex() / 100;
        int roomWidth = roomCoordinates.get(0).getColIndex() / 100;

        searchSpace = new Node[roomHeight + 1][roomWidth + 1];
        notVisitedNodes = new LinkedList<>();
        visitedNodes = new HashSet<>();

        initSearchSpace();

        //delete room width and height -> only showcases left.
        roomCoordinates.remove(0);
        blockNodesWithShowCase(roomCoordinates);
    }

    /**
     * Initialisiert den eigentlichen 2DRaum.
     * Dadurch wird jedem Punkt im 2DRaum ein Node Datenstruktur zugeteilt.
     *
     * Der Startpunkt wird hier ebenfalls initialisiert.
     */
    private void initSearchSpace() {
        int rowIndex = 0;
        int searchSpaceHeight = searchSpace.length;
        int searchSpaceWidth = searchSpace[searchSpaceHeight - 1].length;
    
        while (rowIndex < searchSpaceHeight) {
            int columnIndex = 0;

            while (columnIndex < searchSpaceWidth) {
                Node node = new Node(rowIndex, columnIndex);
                searchSpace[rowIndex][columnIndex] = node;

                addWallNodesToNotVisitedList(rowIndex, searchSpaceHeight, searchSpaceWidth, columnIndex, node);
                columnIndex++;
            }
            rowIndex++;
        }
        previousNode = searchSpace[0][0];
    }

    /**
     * Wenn man sich im 2DRaum an einer Wand befindet werden die Nodes (Spiegel) in die 'notVisitedNodes' hinzugefügt.
     *
     * @param rowIndex die spezifische Reihe an der sich das Element befindet.
     * @param searchSpaceHeight die Höhe des gesamten 2DRaumes
     * @param searchSpaceWidth die Breite des gesamten 2DRaumes
     * @param columnIndex die spezifische Spalte an der sich das Element befindet.
     * @param node das Knotenelement, welches in die 'notVisitedNode' hinzugefügt werden soll.
     */
    private void addWallNodesToNotVisitedList(int rowIndex, int searchSpaceHeight, int searchSpaceWidth, int columnIndex, Node node) {
        if (columnIndex == searchSpaceWidth - 1 || columnIndex == 0 || rowIndex == searchSpaceHeight - 1 || rowIndex == 0) {
            notVisitedNodes.add(node);
        }
    }

    /**
     * Blockt die nötigen Knoten im 2DRaum. Hier werden also die Vitrinen im Raum gesetzt.
     *
     * @param showCases Vitrine
     */
    private void blockNodesWithShowCase(List<Node> showCases) {
        for (Node showCase : showCases) {
            searchSpace[showCase.getRowIndex() / 100][showCase.getColIndex() / 100].setBlocked(true);
        }
    }

    /**
     * Der eigentliche Suchalgorithmus ist hier.
     *
     * Wir iterieren hier nur durch die Wände und ignorieren die inneren Felder komplett, da diese für unser Endergebnis
     * irrelevant sind. Um weitere Berechnungen zu vermeiden, wurde diese nicht berücksichtigt.
     *
     * Wir zählen erst den Index <code>Y hoch</code>, sobald dieser das Ende erreicht hat, wird Index <code>X hoch</code>
     * hochgezählt. Ist dieser wiederum am Ende angelagt, zählen wir <code>Y runter</code>. Ist dieser wieder bei <code>0</code>
     * wird der Index für die <code>X-Koordinate runtergezählt</code>.
     *
     * Damit wird erreicht, dass wir quasi einen <i>Kreis</i> im Raum machen.
     *
     * @return Node - unser neues Ziel.
     */
    private Node getNextTargetNodeInSearchSpace() {
        int rowIndexStart = targetNode != null ? targetNode.getRowIndex() : 0;
        int colIndexStart = targetNode != null ? targetNode.getColIndex() : 0;

        int searchSpaceHeight = searchSpace.length - 1;
        int searchSpaceWidth = searchSpace[searchSpaceHeight - 1].length - 1;

        int colIndex = colIndexStart;
        int rowIndex = rowIndexStart;

        while (colIndex <= searchSpaceWidth && colIndex >= 0) {
            while (rowIndex <= searchSpaceHeight && rowIndex >= 0) {
                if (isValidPath(searchSpace[rowIndex][colIndex])) {
                    targetNode = searchSpace[rowIndex][colIndex];

                    if (!isPathFree()) {
                        countingY = !countingY;
                    }
                    notVisitedNodes.remove(targetNode);
                    return targetNode;
                }

                if (countingY) {
                    rowIndex += yMultiplier;
                } else {
                    colIndex += xMultiplier;
                }

                if (colIndex > searchSpaceWidth) {
                    countingY = !countingY;
                    xMultiplier *= -1;
                    colIndex = searchSpaceWidth;
                }
                if (colIndex < 0) {
                    countingY = !countingY;
                    xMultiplier *= -1;
                    colIndex = 0;
                }

                if (rowIndex > searchSpaceHeight) {
                    countingY = !countingY;
                    yMultiplier *= -1;
                    rowIndex = searchSpaceHeight;
                }

                if (rowIndex < 0) {
                    countingY = !countingY;
                    yMultiplier *= -1;
                    rowIndex = 0;
                }
            }
        }
        return targetNode;
    }

    /**
     * Hilfsmethode für die Überprüfung ob der Pfad gütlig ist oder nicht.
     * @param currentNode der Knoten indem sich der Algorithmus gerade befindet.
     * @return true wenn der Pfad nicht blockiert ist.
     */
    private boolean isValidPath(Node currentNode) {
        return !isTargetNodeParallelToWall(currentNode)
                && targetNodeIstAtWall(currentNode)
                && !visitedNodes.contains(currentNode);
    }

    /**
     * Gibt <code>true</code> oder <code>false</code> zurück,
     * wenn der aktuelle Knoten parallel zum vorherigen Knoten ist oder auf der gleichen Höhen sind.
     *
     * @param currentNode der Knoten indem sich der Algorithmus gerade befindet.
     * @return <code>true</code> wenn beide an der selben Wand sind.
     */
    public boolean isTargetNodeParallelToWall(Node currentNode) {
        boolean atSameWallWith = currentNode.isAtSameWallWith(previousNode, searchSpace);
        return targetNode == null ? atSameWallWith : atSameWallWith || isSameCol(currentNode) || isSameRow(currentNode);
    }

    /**
     * Evaluiert ob der vorherige Knoten mit dem aktuellen Knoten auf einer Reihe ist.
     * Es wird vermieden einen "geraden" Schuss auf die entgegengesetzte Wand zu schießen.
     *
     * @param currentNode der Knoten indem sich der Algorithmus gerade befindet.
     * @return <code>true</code> wenn beide auf der selben Reihe sind.
     */
    private boolean isSameRow(Node currentNode) {
        return previousNode.getRowIndex() == currentNode.getRowIndex();
    }

    /**
     * Evaluiert ob der vorherige Knoten mit dem aktuellen Knoten auf einer Spalte ist.
     * Es wird vermieden einen "geraden" Schuss auf die entgegengesetzte Wand zu schießen.
     *
     * @param currentNode der Knoten indem sich der Algorithmus gerade befindet.
     * @return <code>true</code> wenn beide auf der selben Spalte sind.
     */
    private boolean isSameCol(Node currentNode) {
        return previousNode.getColIndex() == currentNode.getColIndex();
    }

    /**
     * Berechnet ob der berechnete targetNode tatsächlich an einer Wand is.
     *
     * @param currentNode der Knoten indem sich der Algorithmus gerade befindet.
     * @return <code>true</code> wenn eine Wand gefunden wurde.
     */
    private boolean targetNodeIstAtWall(Node currentNode) {
        return currentNode.isAtWall(searchSpace);
    }

    /**
     * Berechnet die Schnittpunkte mit einer Vitrine und sagt letzendlich ob der Weg für den Laser frei ist oder nicht.
     *
     * @return <code>true</code> wenn <code>KEINE</code> Vitrinen vom Laser geschnitten werden.
     */
    private boolean isPathFree() {
        if (targetNode == null) {
            return true;
        }

        Set<Node> showCases = extractShowCases();

        Node showCase = showCases.iterator().next(); // TODO put in loop?

        Line2D laser = new Line2D.Double(
                previousNode.getColIndex(),
                previousNode.getRowIndex(),
                targetNode.getColIndex(),
                targetNode.getRowIndex()
        );

        boolean intersects = laser.intersectsLine(showCase.getColIndex(), showCase.getRowIndex(), showCase.getColIndex() + 1, showCase.getRowIndex() + 1);

        return !intersects && !targetNode.isBlocked();
    }

    /**
     * Hilfmethode um alle Vitrinen-Koordinaten zu finden.
     * @return Set von Vitrinenpunkten im 2DRaum.
     */
    private Set<Node> extractShowCases() {
        Set<Node> showCases = new HashSet<>();

        for (Node[] aSearchSpace : searchSpace) {
            for (Node currentNode : aSearchSpace) {
                if (currentNode.isBlocked()) {
                    showCases.add(currentNode);
                }
            }
        }
        return showCases;
    }

    /**
     * Entfern die bereits <code>nicht</code> besuchten Knoten von der notVisitedNodes Liste und evaluiert den nächten Zielpunkt.
     * Fügt ebenfalls Node in die bereits besuchten Knoten Liste hinzu.
     */
    public void searchForPathInSearchSpace() {
        while (!notVisitedNodes.isEmpty()) {
            if (targetNode != null) {
                previousNode.setColIndex(targetNode.getColIndex());
                previousNode.setRowIndex(targetNode.getRowIndex());

                //this is a workaround of a bug i don't know how to solve yet...
                //just ignore this... please...
                if ((previousNode == targetNode) || notVisitedNodes.size() == 1) {
                    break;
                }
            }

            targetNode = getNextTargetNodeInSearchSpace();
            System.out.println("Über: " + targetNode);

            visitedNodes.add(targetNode);

        }
    }

}
